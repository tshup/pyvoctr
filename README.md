### About Python Dictionary Trainer ###

The entire purpose of the application is to display words from a dictionary in a post-it-like window.

Features:
  * Pin on top of other windows (`Always on Top` in context menu)
  * Hide window decorations (`Hide window frame`)
  * Show translated phrase after a configurable delay  (`Set think time`)
  * Change background color (`Set background color`)
  * Click to advance to next word
  * HTML support in definitions
  * Very simple dictionary format
Example dictionary file:
```
el perro = dog
la bombilla = bulb
```
To use your own dictionary, save the file with a `.txt` extension and open it via right-click > Open dictionary.

### Adding pictures ###
You can add HTML tags to the text of entries:
Example:
```
una toalla = <img src='towell.jpg'><br>towell
```

Example:

![](pyvoctr/pyvcontr_card.png)

### Installation on Windows ###
Download and run the [installer](http://pyvoctr.googlecode.com/files/pyvoctr_setup.exe). A launcher icon will be created in your Start Menu under `Python Vocabulary Trainer`.

### Installation on Linux ###
Clone the project and run `python pyvoctr.py`. 

The app uses the PyQt4 library so that will have to be installed using your package manager.

To install PyQt4 on ubuntu/debian:

```
sudo apt-get install python-qt4
```

### Similar Projects ###

If you are looking for a flashcard or vocabulary trainer app with more features, have a look at the following projects:

  * [anki](http://ichi2.net/anki)
  * [mnemosyne](http://mnemosyne-proj.sourceforge.net)
